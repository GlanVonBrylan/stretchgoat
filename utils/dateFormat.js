"use strict";

Number.prototype.pad2 = function() { return (""+this).padStart(2, 0); }

Date.prototype.oldToLocaleString = Date.prototype.toLocaleString;
Date.prototype.toLocaleString = function(lang) {
	return lang === "fr" || lang === "FR"
		? formatDate("DD/MM/YYYY à HH:mm", this)
		: this.oldToLocaleString(lang);
}

exports.formatDate = formatDate;
function formatDate(format, date = new Date())
{
	if(format instanceof Date)
	{
		const temp = format;
		format = date;
		date = format;
	}
	if(!(date instanceof Date))
		date = new Date(date);

	const vYearLong = date.getFullYear().toString();
	const vYearShort = vYearLong.substring(2, 4);
	return format
		.replaceAll("DD", date.getDate().pad2())
		.replaceAll("MM", (date.getMonth() + 1).pad2())
		.replaceAll("YYYY", vYearLong)
		.replaceAll("YY", vYearShort)
		.replaceAll("HH", date.getHours().pad2())
		.replaceAll("mm", date.getMinutes().pad2())
		.replaceAll("ss", date.getSeconds().pad2())
		.replaceAll("SSS", (""+date.getMilliseconds()).padStart(3, 0));
}