"use strict";

require("discord.js").CommandInteraction.prototype.ephReply = function(content) {
	return this.reply({flags: "Ephemeral", ...(typeof content === "object" ? content : {content})});
}
