"use strict"

Object.assign(Array.prototype, {
	random: function() { return this[~~(Math.random() * this.length)]; },

	extractRandom: function() {
		const index = ~~(Math.random() * this.length);
		const element = this[index];
		this.splice(index, 1);
		return element;
	},

	shuffle: function() {
		for(let i = this.length - 1 ; i > 0 ; i--)
		{
			const j = ~~(Math.random() * (i + 1));
			[this[i], this[j]] = [this[j], this[i]];
		}
		return this;
	},
});
