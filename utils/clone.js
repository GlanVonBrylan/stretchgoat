
module.exports = clone;

function clone(obj)
{
	"use strict";

	if(obj === null || typeof obj !== "object")
		return obj;

	const newObj = new obj.constructor();

	for(const key in obj)
		newObj[key] = clone(obj[key]);

	return newObj;
}
