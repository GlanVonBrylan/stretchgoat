"use strict";

const { existsSync } = require("fs");

module.exports = {
	getEmojiDiscriminator: (emoji) => emoji.id ? `${emoji.name}:${emoji.id}` : emoji.name,

	cleanEmojiDiscriminator: (emojiDiscriminator) => {
		const cleaned = /[A-Za-z0-9_]+:[0-9]+/.exec(emojiDiscriminator);
		if (cleaned) return cleaned[0];
		return emojiDiscriminator;
	},
};
