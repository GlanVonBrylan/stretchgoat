"use strict";

String.prototype.replaceLast = function(search, replace) {
	const index = this.lastIndexOf(search);
	return index === -1 ? this : this.slice(0, index) + replace + this.slice(index + search.length);
}
