"use strict";

const Discord = require("discord.js");
const INTENTS = Discord.GatewayIntentBits;

require("./utils/CommandInteraction");

// instance de notre client Discord (bot)
global.bot = module.exports = exports = Object.assign(new Discord.Client({
	allowedMentions: {parse: ["users", "roles"]},
	intents: [
		INTENTS.Guilds,
		INTENTS.GuildMembers,
		INTENTS.GuildMessages, //INTENTS.MessageContent,
		INTENTS.GuildMessageReactions,
	],
}), {
	config: require("./data/settings.json"),
	root: __dirname,

	roles: require("fs").existsSync("data/roles.json") ? require("./data/roles.json") : {},

	//armes: require("./data/weapons.json"),
	//_actions: require("./data/actions.json"), // do no replace Client#actions !
	activities: ["effrayer les explorateurs", "spoiler les explorateurs", "brouter les hautes herbes", "au 7th Continent", "hanter le continent", "mélanger les cartes terrain", "lancer des malédictions", "s'étirer et bailler à goat déployée.",],
});

// On convertit les couleurs en nombres
for(const [key, value] of Object.entries(bot.config))
	if(value.match?.(/^#[0-9A-Fa-f]{6}$/))
		bot.config[key] = Number(value.replace("#", "0x"));

// This method was removed in Discord.js v14;
require("discord.js").EmbedBuilder.prototype.addField = function(name, value, inline = false) {
	return this.addFields({name, value, inline});
}

Object.assign(bot, require("./utils/functions"));
Object.assign(global, require("@brylan/djs-commands").enums);

for(const file of require("fs").readdirSync("events/")) {
	const eventName = file.slice(0, -3);
	bot.on(eventName, require(`./events/${file}`));
}

bot.login(bot.config.bot_token);
