"use strict";

exports.description = "Explique comment mettre en forme du texte sur Discord";
exports.run = inter => {
	const en = inter.locale.startsWith("en");
	inter.ephReply({embeds: [{
		title: en ? "Text formatting on Discord" : "Mise en forme du texte sur Discord",
		color: bot.config.purple,
		thumbnail: { url: inter.guild.iconURL() },
		fields: en ? fields_en : fields,
	}]});
}

const fields = [
	{ name: "*Italique*", value: "```*Italique*```" },
	{ name: "**Gras**", value: "```**Gras**```" },
	{ name: "***Italique & Gras***", value: "```***Italique & Gras***```" },
	{ name: "__Souligné__", value: "```__Souligné__```" },
	{ name: "__*Italique & Souligné*__", value: "```__*Italique & Souligné*__```" },
	{ name: "__**Gras & Souligné**__", value: "```__**Gras & Souligné**__```" },
	{ name: "__***Italique, Gras & Souligné***__", value: "```__***Italique, Gras & Souligné***__```" },
	{ name: "~~Barré~~", value: "```~~Barré~~```" },
	{ name: "||Cacher un texte||", value: "```||Cacher un texte||```" },
];

const fields_en = [
	{ name: "*Italics*", value: "```*Italics*```" },
	{ name: "**Bold**", value: "```**Bold**```" },
	{ name: "***Italics & Bold***", value: "```***Italics & Bold***```" },
	{ name: "__Underlined__", value: "```__Underlined__```" },
	{ name: "__*Italics & Underlined*__", value: "```__*Italics & Underlined*__```" },
	{ name: "__**Bold & Underlined**__", value: "```__**Bold & Underlined**__```" },
	{ name: "__***Italics, Bold & Underlined***__", value: "```__***Italics, Bold & Underlined***__```" },
	{ name: "~~Striked~~", value: "```~~Striked~~```" },
	{ name: "||Hide text||", value: "```||Hide text||```" },
];



exports.localization = {
	name: "formatting", description: "Explains how to format text on Discord",
};
