"use strict";

const { formatDate } = require("../../../utils/dateFormat");
const { existsSync, readFileSync, promises: {writeFile} } = require("fs");
const { MessageFlags: {SuppressNotifications: SUPPRESS_NOTIFICATIONS} } = require("discord.js");

const bot = require("../../../bot");
const annoncesFile = __dirname + "/../../../data/annonces.json";
const annonces = exports.annonces = {};

Object.defineProperty(annonces, "toChoices", {value: function() {
	const annonces = Object.values(this);
	return annonces.length
		? annonces.map(({ id, titre, date }) => {
			titre = `(${formatDate("DD/MM HH:mm", date)}) ${titre}`;
			return {
				value: "" + id,
				name: titre.length > 32 ? `${titre.substring(0, 31)}…` : titre
			};
		})
		: [{name: "Il n'y a aucune annonce.", value: "/"}];
}})


if(existsSync(annoncesFile))
{
	Object.assign(annonces, JSON.parse(readFileSync(annoncesFile, "utf-8")));
	exports.onReady = async () => {
		for(const annonce of Object.values(annonces))
		{
			if(typeof annonce.salon === "string")
				annonce.salon = await bot.channels.fetch(annonce.salon);

			annonce.timeout = setTimeout(send, annonce.date - Date.now(), annonce);
		}
	};
}

let saving = false;
let postSave = Function();
function saveAnnonces()
{
	if(saving)
	{
		if(saving !== "timeoutScheduled")
		{
			saving = "timeoutScheduled";
			setTimeout(saveAnnonces, 500);
		}
		return;
	}

	const savable = {};
	for(const [id, annonce] of Object.entries(annonces))
		savable[id] = { ...annonce, salon: annonce.salon.id, timeout: null };

	saving = true;
	writeFile(annoncesFile, JSON.stringify(savable, null, 4))
		
		.finally(() => saving = false);
	postSave();
}

exports.setPostSave = function(func)
{
	if(typeof func !== "function")
		throw new TypeError("'func' must be a function");
	postSave = func;
}


exports.ajoute = function(annonce)
{
	if(annonce.date === "now")
		send(annonce);
	else
	{
		annonces[annonce.id] = annonce;
		annonce.timeout = setTimeout(send, annonce.date - Date.now(), annonce);
		saveAnnonces();
	}
}

exports.deplace = function(id, date)
{
	if(id in annonces)
	{
		const annonce = annonces[id];
		clearTimeout(annonce.timeout);
		annonce.date = date;
		exports.ajoute(annonce);
		return true;
	}
	return false;
}

exports.annule = function(id)
{
	if(id in annonces)
	{
		clearTimeout(annonces[id].timeout);
		delete annonces[id];
		saveAnnonces();
		return true;
	}
	return false;
}


const { ChannelType: {GuildAnnouncement: GUILD_NEWS} } = require("discord.js");

var lastSent = {};

function send(annonce)
{
	const embed = {
		title: annonce.titre,
		description: annonce.texte,
	};

	if(annonce.urlImage)
		embed.image = { url: annonce.urlImage };

	// temporaire
	if(annonce.titre === lastSent.titre && annonce.description === lastSent.description && annonce.urlImage === lastSent.image?.url)
		return console.warning("Zut j'ai essayé d'envoyer à nouveau :", annonce.id, annonce.titre);
	else
		lastSent = embed;

	const toSend = { embeds: [embed], allowedMentions: {parse: ["users", "roles", "everyone"]} };
	if(annonce.everyone)
		toSend.content = annonce.everyone;
	if(annonce.silent)
		toSend.flags = SUPPRESS_NOTIFICATIONS;

	annonce.salon.send(toSend)
		.then(message => {
			if(annonce.salon.type === GUILD_NEWS)
				message.crosspost();
			console.log("Annonce confirmée :", annonce.id, annonce.titre, Date.now());
		})
		;
	console.log("Annonce envoyée :", annonce.id, annonce.titre, Date.now());
	delete annonces[annonce.id];
	saveAnnonces();
}