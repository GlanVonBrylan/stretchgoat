"use strict";

const DEFAULT_EMOJI = "🎁";

const giveaways = require("../../../data/DataSaver")("giveaways", { channel: "", end: 0, prizes: [], emoji: DEFAULT_EMOJI });
const bot = require("../../../bot");
const timeouts = {};

for(const [id, {end}] of giveaways.loadAll())
	timeouts[id] = setTimeout(endGiveaway.bind(null, id), end - Date.now());


bot.on("messageDelete", msg => {
	if(giveaways.has(msg.id) && giveaways.get(msg).channel === msg.channel.id)
		deleteGiveaway(msg.id);
});


module.exports = exports = {
	distribute, groupPrizes,
	createGiveaway, deleteGiveaway, endGiveaway,
	DEFAULT_EMOJI,
};


function createGiveaway(msg, end, prizes, emoji, color)
{
	const customEmojiId = emoji.match(/[0-9]+(?=>)/);
	if(customEmojiId)
		emoji = customEmojiId[0];

	giveaways.set(msg, { channel: msg.channel.id, end, prizes, emoji, color });
	timeouts[msg.id] = setTimeout(endGiveaway.bind(null, msg.id), end - Date.now());
}

function deleteGiveaway(msgId)
{
	clearTimeout(timeouts[msgId]);
	delete timeouts[msgId];
	giveaways.delete({id: msgId});
}

async function endGiveaway(msgId)
{
	const {channel: channelId, prizes, emoji, color} = giveaways.get({id: msgId});
	deleteGiveaway(msgId);
	const channel = await bot.channels.fetch(channelId).catch(Function());
	if(!channel) return;
	const message = await channel.messages.fetch(msgId).catch(Function());
	if(!message) return;
	let {users} = message.reactions.cache.get(emoji);

	// Fetch can get at most 100 users, so prefer the cache if possible
	// Ideally, the bot is not restarted while a giveaway is going...
	users = users.cache.size > 50 ? users.cache : await users.fetch();
	users = [...users.values()].filter(({bot}) => !bot);

	if(!users.length)
		return channel.send({ embeds: [{
			title: "Oups...",
			description: `... personne n’a participé au [giveaway](${message.url}).`
		}]}).then(({url}) => message.edit("**Giveaway terminé.** Résultats : "+url));

	const distribution = distribute(prizes, users);
	const embed = { color };

	if(!prizes.tooMany && prizes.length === 1)
	{
		embed.title = `Et le ${prizes[0]} est remporté...`;
		embed.description = `... par ${distribution} ! Félicitations !`;
	}
	else
	{
		embed.title = "Tirage terminé !";
		embed.description = `Résultats du [giveaway](${message.url})`;
		embed.fields = Object.entries(distribution).map(([userId, prize]) => ({ name: prize, value: `<@${userId}>`, inline: true }));
	}

	if(prizes.tooMany)
		embed.footer = { text: `Par manque de participants, les prix suivants n’ont pas été distribués : ${groupPrizes(prizes)}` };

	channel.send({
		content: "@everyone",
		embeds: [embed],
		allowedMentions: {parse: ["users", "roles", "everyone"]},
	})
	.then(({url}) => message.edit("**Giveaway terminé.**\nRésultats : "+url))
	;
}


function distribute(prizes, users, distributeExcess = false)
{
	if(prizes.length === 1)
	{
		const winner = users.random();
		prizes.losers = users.filter(u => u !== winner);
		return winner;
	}

	const distribution = {};

	if(users.length < prizes.length)
	{
		prizes.tooMany = true;
		if(distributeExcess)
		{
			users.shuffle();
			for(let i = 0 ; i < prizes.length ; i++)
			{
				const {id} = users[i % users.length];
				if(!(id in distribution))
					distribution[id] = [];
				distribution[id].push(prizes[i]);
			}
		}
		else
		{
			prizes.shuffle();
			for(const user of users)
				distribution[user.id] = prizes.pop();
		}
	}
	else
	{
		if(users.length / 3 > prizes.length)
		{
			let id;
			for(const prize of prizes)
			{
				do {
					({id} = users.random());
				} while(distribution[id]);
				distribution[id] = prize;
			}
		}
		else
		{
			users.shuffle();
			let i = -1;
			for(const prize of prizes)
				distribution[users[++i].id] = prize;

			if(users.length !== prizes.length)
				prizes.losers = users.slice(i+1);
		}
	}

	return distribution;
}


function groupPrizes(prizes)
{
	const groupedPrizes = {};
	for(const prize of prizes)
	{
		if(groupedPrizes[prize])
			groupedPrizes[prize]++;
		else
			groupedPrizes[prize] = 1;
	}
	return Object.defineProperty(groupedPrizes, "toString", { value: groupedPrizes_toString });
}

function groupedPrizes_toString()
{
	return Object.entries(this).map(([prize, n]) => `${n}${n === 1 ? ' ' : '×'}${prize}`).join(", ").replaceLast(",", " et")
}
