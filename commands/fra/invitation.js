"use strict";

exports.description = "Afficher le lien d'invitation du serveur";
exports.run = inter => {
	const en = inter.locale.startsWith("en");
	inter.reply({embeds: [{
		description: en ? "__Official link for the Discord__" : "__Lien officiel du Discord__",
		color: bot.config.blue,
		fields: [{ name: en ? "Copy the URL below" : "Copiez l'URL ci-dessous", value: bot.config.invite_url }],
	}]});
}


exports.localization = {
	name: "invite", description: "Show the official invite for the server",
};
