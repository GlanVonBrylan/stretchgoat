"use strict";

const fetch = require("node-fetch");
const {
	annonces,
	onReady, setPostSave,
	ajoute, deplace, annule
} = require("./#functions/annonces");
exports.onReady = onReady;

const MSG_LINK = /discord\.com\/channels\/[0-9]+\/([0-9]+)\/([0-9]+)/;

const {
	ChannelType: {GuildText: GUILD_TEXT, GuildAnnouncement: GUILD_NEWS},
	PermissionFlagsBits: {SendMessages: SEND_MESSAGES},
} = require("discord.js");

exports.description = "Gérer les annonces";
exports.defaultMemberPermissions = "0";
exports.options = [{
	type: SUBCOMMAND, name: "envoyer",
	description: "Envoyer une annonce",
	options: [{
		type: CHANNEL, name: "salon", required: true,
		channelTypes: [GUILD_TEXT, GUILD_NEWS],
		description: "Le salon où envoyer l'annonce",
	}, {
		type: STRING, name: "titre", required: true,
		description: "Titre de l'annonce",
	}, {
		type: ATTACHMENT, name: "annonce", required: true,
		description: "Fichier contenant le texte de l'annonce.",
	}, {
		type: STRING, name: "date-et-heure",
		description: "Quand envoyer l'annonce, au format aaaa-mm-jj hh:mm. Si laissé vide elle sera envoyée immédiatement.",
	}, {
		type: STRING, name: "mention",
		description: "S'il faut mentionner @everyone ou @here. Peut être laissé vide.",
		choices: [{name: "@everyone", value: "@everyone"}, {name: "@here", value: "@here"}],
	}, {
		type: STRING, name: "silencieux",
		description: "Si la mention doit être silencieuse.",
		choices: [{name: "@silent", value: "@silent"}],
	}, {
		type: STRING, name: "url-image",
		description: "URL de l'image (incompatible avec 'image')",
	}, {
		type: ATTACHMENT, name: "image",
		description: "Fichier à utiliser pour l'image (incompatible avec 'url-image')",
	}]
}, {
	type: SUBCOMMAND, name: "deplace",
	description: "Changer la date et heure d'une annonce.",
	options: [{
		type: STRING, name: "id", required: true,
		description: "identifiant de l'annonce à déplacer",
		choices: annonces.toChoices(),
	}, {
		type: STRING, name: "date-et-heure",
		description: "Quand envoyer l'annonce, au format aaaa-mm-jj hh:mm. Si laissé vide elle sera envoyée immédiatement.",
	}],
}, {
	type: SUBCOMMAND, name: "supprime",
	description: "Annule une annonce prévue",
	options: [{
		type: STRING, name: "id", required: true,
		description: "identifiant de l'annonce à supprimer",
		choices: annonces.toChoices(),
	}],
}, {
	type: SUBCOMMAND, name: "liste",
	description: "Liste les annonces prévues",
}, {
	type: SUBCOMMAND, name: "corrige",
	description: "Corrige une annonce déjà envoyée. Les champs laissés vides seront inchangés.",
	options: [{
		type: STRING, name: "lien-message", required: true,
		description: "Le lien du message à modifier",
	}, {
		type: STRING, name: "titre",
		description: "Nouveau titre de l'annonce",
	}, {
		type: ATTACHMENT, name: "annonce",
		description: "Fichier contenant le nouveau texte de l'annonce.",
	}, {
		type: STRING, name: "url-image",
		description: "Nouvelle URL de l'image",
	}],
}];
exports.run = async inter => {
	const {options} = inter;
	const command = options.getSubcommand();

	if(command === "liste")
	{
		const _annonces = Object.values(annonces);
		return inter.reply(_annonces.length
			? {embeds: [{
				title: "Liste des annonces prévues",
				fields: _annonces.map(annonce => ({name: annonce.titre, value: `id : \`${annonce.id}\`\nle ${new Date(annonce.date).toLocaleString("fr")}\ndans ${annonce.salon}`}))}]}
			: "Aucune annonce n'est prévue."
		);
	}

	if(command === "deplace")
	{
		const id = options.getString("id");
		if(id === "/")
			inter.reply("Il n'y a aucune annonce prévue.");
		const date = parseDate(options.getString("date-et-heure"));
		if(date instanceof Promise)
			return date;
		else if(deplace(id, date))
			inter.reply("Annonce déplacée.");
		else
			inter.reply("Il n'y avait pas d'annonce avec cet identifiant.");
		
		return;
	}

	if(command === "supprime")
	{
		const id = options.getString("id");
		if(id === "/")
			inter.reply("Il n'y a aucune annonce prévue.");
		else if(annule(id))
			inter.reply("Annonce supprimée.");
		else
			inter.reply("Il n'y avait pas d'annonce avec cet identifiant.");

		return;
	}
	
	
	function parseDate(date) {
		if(!date)
			return "now";
		
		date = Date.parse(date);
		if(!date)
			return inter.ephReply("Format de date et heure invalide.");
		
		const timeout = date - Date.now();
		if(timeout < 0)
			return inter.ephReply("Cette date est dans le passé.");

		if(timeout >= 2**31) // Timeouts are limited to those that fit in a 32-bit signed integer
			return inter.ephReply("À cause de limitations de JavaScript, on ne peut pas prévoir d'annonce plus de 24 jours à l'avance.");
		
		return date;
	}
	async function getText(ephemeralDefer = false) {
		let texte = options.getAttachment("annonce");
		const { contentType } = texte;
		if(!contentType.startsWith("text"))
			return inter.ephReply("Vous devez fournir un fichier texte.");
		if(!texte.size)
			return inter.ephReply(`Le fichier est vide.`);
		if(texte.size > 2048)
			return inter.ephReply(`Le texte ne peut pas faire plus de 2048 caractères (${texte.size} reçus).`);
	
		await inter.deferReply({ ephemeral: ephemeralDefer }).catch(console.error);
		const response = await fetch(texte.url);
		if(response.status >= 400)
			return inter.editReply(`Erreur en récupérant le fichier texte : ${response.statusText}`);

		const buffer = await response.arrayBuffer();
		const encodingPos = contentType.indexOf("charset=");
		const encoding = encodingPos !== -1
			? contentType.slice(encodingPos + "charset=".length)
			: "utf-8";
		let text = new TextDecoder(encoding).decode(buffer);
		if(text.includes(String.fromCodePoint(0xfffd)))
			text = new TextDecoder("latin1").decode(buffer);
		return text;
	}

	if(command === "corrige")
	{
		const link = options.getString("lien-message").match(MSG_LINK);
		if(!link)
			return inter.ephReply("Vous devez fournir un lien de message.");

		const [ , channelId, messageId] = link;
		const channel = await inter.guild.channels.fetch(channelId).catch(console.error);
		if(!channel)
			return inter.ephReply("Lien de message invalide (salon non trouvé).");
		const message = await channel.messages.fetch(messageId).catch(console.error);
		if(!message)
			return inter.ephReply("Lien de message invalide (message non trouvé).");

		if(!message.editable)
			return inter.ephReply(`Je ne peux pas modifier ce message. ${message.url}`);

		if(!message.embeds.length)
			return inter.ephReply("Ce message n'est pas une annonce.");
		
		const original = message.embeds[0].data;
		const embed = { ...original };
		const titre = options.getString("titre")?.trim();
		if(titre)
		{
			if(titre.length > 256)
				return inter.ephReply(`Le titre ne peut pas faire plus de 256 caractères (${titre.length} reçus).`);
			embed.title = titre;
		}
		
		const urlImage = options.getString("url-image");
		if(urlImage)
		{
			if(!urlImage.startsWith("http"))
				return inter.ephReply("L'URL de l'image est invalide.");

			embed.image = { url: urlImage };
		}

		if(options.get("annonce"))
		{
			const texte = await getText(true);
			if(typeof texte !== "string")
				return;
			embed.description = texte;
		}
		else
			await inter.deferReply({ flags: "Ephemeral" }).catch(console.error);
		
		if(embed.title === original.title
			&& embed.description === original.description
			&& embed.image === original.image
		) return inter.editReply("Vous n'avez rien modifié...");

		message.edit({ content: message.content, embeds: [embed] })
			.then(() => inter.editReply(`Message modifié. ${message.url}`),
			err => {
				console.error(err);
				inter.editReply(`Le modification du message a échoué :\n${err.message}`);
			});
		return;
	}


	let [titre, date, everyone = "", silent, urlImage]
		= ["titre", "date-et-heure", "mention", "silencieux", "url-image"].map(o => options.getString(o));

	date = parseDate(date);
	if(date instanceof Promise)
		return date;

	const salon = options.getChannel("salon");
	if(!salon.permissionsFor(bot.user).has(SEND_MESSAGES))
		return inter.ephReply("Je ne peux pas écrire dans ce salon.");

	if(urlImage && !urlImage.startsWith("http"))
		return inter.ephReply("L'URL de l'image est invalide.");

	const image = options.getAttachment("image");
	if(image)
		urlImage = image.url;

	titre = titre.trim();
	if(titre.length > 256)
		return inter.ephReply(`Le titre ne peut pas faire plus de 256 caractères (${titre.length} reçus).`);

	const texte = await getText();
	if(typeof texte !== "string")
		return;

	const id = Date.now();
	const annonce = { id, salon, titre, urlImage, texte, date, everyone, silent: !!silent };

	ajoute(annonce);
	inter.editReply(date === "now"
		? `Annonce envoyée dans ${salon}`
		: `Annonce "${titre}" enregistrée avec l’identifiant \`${id}\`.`
	);
}



function getCmd(cmdName) {
	return exports.options.find(({name}) => name === cmdName);
}
setPostSave(() => {
	const choices = annonces.toChoices();
	getCmd("deplace").options[0].choices = choices;
	getCmd("supprime").options[0].choices = choices;
	exports.self.edit(exports);
});
