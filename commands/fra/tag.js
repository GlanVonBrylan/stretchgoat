"use strict";

const {tags} = require("./tags");

exports.description = "Envoyer un tag";
exports.defaultMemberPermissions = "0";
exports.options = [{
	type: STRING, name: "tag", required: true,
	description: "Le nom du tag à envoyer",
	choices: tags.toChoices(),
}];
exports.run = inter => {
	const tag = inter.options.getString("tag");
	if(tag === "/")
		inter.ephReply("Il n'y a aucun tag pour l'instant.");
	else if(tag in tags)
		inter.reply(tags[tag]);
	else
		inter.ephReply("Ce tag n'existe pas.");
}


exports.updateTags = () => {
	exports.options[0].choices = tags.toChoices();
	exports.self.edit(exports);
}
