"use strict";

const {version} = JSON.parse(require("fs").readFileSync("package.json"));

exports.description = "Afficher les infos sur le bot";
exports.run = async inter => {
	const app = await bot.application.fetch();

	inter.reply({embeds: [inter.locale.startsWith("en") ? {
		description: "Who am I",
		color: bot.config.green,
		thumbnail: { url: bot.user.displayAvatarURL() },
		fields: [
			{ name: "Name", value: bot.user.username },
			{ name: "Created the", value: bot.user.createdAt.toLocaleString(inter.locale) },
			{ name: "by", value: "Khyinn" },
			{ name: "Then maintained and updated by", value: `Morgân von Brylân *(since ${inter.locale === "en-US" ? "01/23/2021" : "23/01/2021"})*` },
			{ name: "Technically, I belong to", value: app.owner.toString() },
		],
		footer: {text: "Version "+version},
	} : {
		description: "Ma carte d'identité",
		color: bot.config.orange,
		thumbnail: { url: bot.user.displayAvatarURL() },
		fields: [
			{ name: "Mon nom", value: bot.user.username },
			{ name: "J'ai été créé le", value: "12/09/2019 à 18:47" },
			{ name: "par", value: "Khyinn" },
			{ name: "Puis maintenu et mis à jour par", value: "Morgân von Brylân *(depuis le 23/01/2021)*" },
			{ name: "Techniquement, j'appartiens à", value: app.owner.toString() },
		],
		footer: { text: "Version "+version },
	}]});
}


exports.localization = {
	name: "bot-info", description: "Display information about the bot",
};
