"use strict";

/* Linked roles */

const { existsSync, promises: {readFile, writeFile}, unlink } = require("fs");

const saveFile = "./data/linkedRoles.json";

class LinkList {
	has(roleA, roleB) {
		return this[roleA]?.includes(roleB);
	}
	add(roleA, roleB) {
		const existing = this[roleA];
		if(existing?.includes(roleB))
			return false;
	
		if(existing)
			existing.push(roleB);
		else
			this[roleA] = [roleB];
		return true;
	}
	remove(roleA, roleB = null) {
		if(!roleB)
		{
			if(roleA in this) return delete this[roleA];
			else return false;
		}
	
		const links = this[roleA];
		if(!links?.includes(roleB))
			return false;
		if(links.length === 1)
			return delete this[roleA];
		links.splice(links.indexOf(roleB), 1);
		return links.length;
	}
	purge(deadRole) {
		delete this[deadRole];
		const links = Object.entries(this).filter(([,roles]) => roles.includes(deadRole));
		for(const [role, linkedRoles] of links)
		{
			if(linkedRoles.length === 1)
				delete this[role];
			else
				linkedRoles.splice(linkedRoles.indexOf(deadRole), 1);
		}
	}
	toString() {
		const list = Object.entries(this)
			.map(([role, linkedRoles]) => `<@&${role}> ⇒ ${linkedRoles.map(r => `<@&${r}>`)}`)
			.join("\n");
		return list || "Aucune association pour l'instant.";
	}
}
const links = new LinkList();
const unlinks = new LinkList();
const LINK   = 1<<0;
const UNLINK = 1<<1;

if(existsSync(saveFile))
	readFile(saveFile, "utf8").then(data => {
		data = JSON.parse(data);
		if("unlinks" in data) {
			Object.assign(links, data.links);
			Object.assign(unlinks, data.unlinks);
		}
		else
			Object.assign(links, data);
	});


var saving = false;
function save()
{
	if(saving) return setTimeout(save, 500);
	saving = true;
	writeFile(saveFile, JSON.stringify({links, unlinks}, null, 2))
		.finally(() => saving = false);
}
function condSave(cond)
{
	if(cond) save();
	return cond;
}


exports.description = "Gérer les rôles liés"
exports.defaultMemberPermissions = "0";
exports.options = [{
	type: SUBCOMMAND, name: "lie",
	description: "Lie deux rôles ; Quand on reçoit/perd le rôle A, le bot donnera/retirera le rôle B",
	options: [{
		type: ROLE, name: "role-a", required: true,
		description: "Le rôle déclencheur",
	}, {
		type: ROLE, name: "role-b", required: true,
		description: "Le rôle qui sera donné/retiré en même temps",
	}, {
		type: INTEGER, name: "type", required: true,
		description: "Le type de lien",
		choices: [
			{ name: "Donner B quand on reçoit A", value: LINK },
			{ name: "Retirer B quand on perd A", value: UNLINK },
			{ name: "Les deux", value: LINK | UNLINK },
		],
	}]
}, {
	type: SUBCOMMAND, name: "delie",
	description: "Retire une association",
	options: [{
		type: ROLE, name: "role-a", required: true,
		description: "Le rôle dont il faut retirer les associations",
	}, {
		type: ROLE, name: "role-b",
		description: "Laissez vide pour retirer toutes les associations du rôle A.",
	}]
}, {
	type: SUBCOMMAND, name: "liste",
	description: "Affiche toutes les associations.",
}];
exports.run = async inter => {
	const roleA = inter.options.getRole("role-a");
	const roleB = inter.options.getRole("role-b");

	switch(inter.options.getSubcommand())
	{
		case "lie":
			if(!roleB.editable)
				return inter.ephReply("Je ne peux pas gérer le second rôle.");

			const type = inter.options.getInteger("type");
			inter.reply(set(roleA.id, roleB.id, type)
				? "Association ajoutée."
				: "Cette association existait déjà."
			);
			break;

		case "delie":
			if(roleB)
			{
				inter.reply(remove(roleA.id, roleB.id)
					? "Association supprimée."
					: "Cette association n'existait pas."
				);
			}
			else
			{
				inter.reply(remove(roleA.id)
					? "Associations supprimées."
					: "Ce rôle n'avait aucune association."
				);
			}
			break;

		case "liste":
			inter.reply({embeds: [{
				title: "Associations d'ajouts",
				description: links.toString(),
			}, {
				title: "Associations de suppression",
				description: unlinks.toString(),
			}]});
			break;
	}
}


Object.assign(exports, { links, unlinks, set, remove, purge });

function set(roleA, roleB, type = LINK | UNLINK)
{
	const link = type & LINK
		? links.add(roleA, roleB)
		: links.remove(roleA, roleB);
	const unlink = type & UNLINK
		? unlinks.add(roleA, roleB)
		: unlinks.remove(roleA, roleB);
	return condSave(link || unlink);
}

function remove(roleA, roleB = null)
{
	return condSave(links.remove(roleA, roleB) || unlinks.remove(roleA, roleB));
}

function purge(deadRole)
{
	links.purge(deadRole);
	unlinks.purge(deadRole);
	save();
}
