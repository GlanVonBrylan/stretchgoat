"use strict";

exports.description = "Afficher les infos sur le serveur";
exports.run = inter => {
	const {guild, member} = inter;
	inter.reply({embeds: [inter.locale.startsWith("en") ? {
		description: "Server info",
		color: bot.config.green,
		thumbnail: { url: message.guild.iconURL() },
		fields: [
			{ name: "Server name", value: message.guild.name },
			{ name: "Created at", value: message.guild.createdAt.toLocaleString("en") },
			{ name: "You joined", value: message.member.joinedAt.toLocaleString("en") },
			{ name: "Members count", value: ""+message.guild.memberCount },
		],
	} : {
		description: "Information du serveur",
		color: bot.config.green,
		thumbnail: { url: guild.iconURL() },
		fields: [
			{ name: "Nom du serveur", value: guild.name },
			{ name: "Créé le", value: guild.createdAt.toLocaleString("fr") },
			{ name: "Vous avez rejoint le", value: member.joinedAt.toLocaleString("fr") },
			{ name: "Nombre total de membres", value: guild.memberCount.toString() },
		]
	}]});
}


exports.localization = {
	name: "server-info", description: "Display information about the server",
};
