"use strict";

const { formatDate } = require("../../utils/dateFormat");
const { getMaxJCJ } = require("./jcj-config");

const jcj = require("../../data/DataSaver")("jcj", {
	dernier_jcj: "undefined",
	n_jcj: 0,
});


exports.description = "Affronter un autre membre du serveur en duel !";
exports.options = [{
	type: USER, name: "adversaire", required: true,
	description: "La personne à affronter",
}]
exports.run = inter => {
	const utilisateur = inter.member;
	const infos = jcj.get(utilisateur);
	const maxJCJ = getMaxJCJ(utilisateur);

	if (maxJCJ !== "infini") {
		if(maxJCJ === 0)
			return inter.reply("Désolé, mais tu es privé de JCJ !");

		const today = formatDate("MM/DD/YYYY");
		if (infos.dernier_jcj !== today) {
			infos.dernier_jcj = today;
			infos.n_jcj = 0;
		} else if(infos.n_jcj >= maxJCJ) {
			return inter.ephReply(`<:sandglass:504308182185803812> - Tu as déjà atteint ta limite pour aujourd'hui, reviens demain pour combattre à nouveau !`);
		}

		infos.n_jcj++;
		jcj.save(utilisateur);
	}

	const adversaire = inter.options.getMember("adversaire");
	let result;

	if (utilisateur === adversaire) {
		result = parseString(reponsesSansJoueur[~~(Math.random() * reponsesSansJoueur.length)], `${utilisateur}`);
	} else {
		const utilisateurChance = Math.random();
		const adversaireChance = Math.random();
		const collateralChance = Math.random();
		if (utilisateurChance >= adversaireChance) {
			result = parseString(reponsesUtilisateurVainqueur[~~(Math.random() * reponsesUtilisateurVainqueur.length)], `${utilisateur}`, `${adversaire}`);
		} else if(collateralChance >= adversaireChance) {
			const members = inter.guild.members.cache;
			let victimeCollaterale;
			do {
				victimeCollaterale = members.random();
			} while (victimeCollaterale === utilisateur || victimeCollaterale === adversaire)

			result = parseString(reponsesCollateral[~~(Math.random() * reponsesCollateral.length)], `${utilisateur}`, `${adversaire}`, `${victimeCollaterale}`);
		} else {
			result = parseString(reponsesAdversaireVainqueur[~~(Math.random() * reponsesAdversaireVainqueur.length)], `${utilisateur}`, `${adversaire}`);
		}
	}

	inter.reply(result);
}


function parseString(str, ...params)
{
	if(typeof str !== "string") throw new TypeError("'str' must be a string");
	return str.replace(/%s[0-9]/g, matchedStr => params[matchedStr[2] - 1]);
}


const reponsesSansJoueur = [
	`%s1 a glissé sur une peau de :banana: et s'est fracassé la tête sur le carrelage. Paix à son âme !`,
	`%s1 a voulu faire un selfie mais a confondu son :iphone: avec un :gun:. Dommage !`,
	`%s1 se fait une égratignure avec un feuille de papier, consulte Doctissimo, et meurt d’un cancer généralisé, d’une embolie pulmonaire et d’une liquéfaction du cerveau tout en se vidant de ses trippes en moins de 14 secondes.`,
	`La foudre tombe sur %s1 et le grille sur place.`,
	`%s1 s'appuie sur la fenêtre, qui était mal fermée, et tombe du 4ème étage. Adieu, %s1....`,
	`C'est l'histoire de %s1 qui rentre dans un café et... **PLOUF**`,
	`Bien embêté, %s1 se retient d'éternuer devant tout le monde en se bouchant le nez, il n'avait pas prévu que la pression de sa boite cranienne serait trop forte, sa tête explose et repeint les murs... Plutôt chouette la nouvelle déco !`
];

const reponsesUtilisateurVainqueur = [
	`%s1 décoche un splendide coup de boule rotatif à %s2 qui est K.O. pour le compte !`,
	`%s1 abat froidement %s2 :gun:.`,
	`%s1 arme son arc, le bande... Vise... Et tire ! %s2 ouvre doucement un œil, et se rend compte que la flèche a transpercé la pomme qu'il portait sur la tête ! Mais qu’est-ce que tu faisais avec une pomme sur la tête, %s2 ?`,
	`%s1 arme son arc, le bande... Vise... Et tire ! %s2 a maintenant une flèche plantée dans le genou.`,
	`%s1 arme son arc, le bande... Vise... Et tire ! La flèche passe 5cm à droite du visage de %s2, qui tremble de tout son corps.`,
	`%s1 arme son arc, le bande... Vise... Et tire ! La flèche transperce le visage de %s2 en plein milieu du nez. %s2 reste quelques secondes debout avant de s'effondrer en arrière.`,
	`Un duel au sabre laser s’engage. %s1, frappe de toutes ses forces, si bien que %s2 n’a d’autre choix de reculer, jusqu’à ce qu’il se retrouve acculé dans un coin de la galerie. Sabre contre sabre,  %s1 regarde %s2 dans les yeux avant d’utiliser la force pour l’étrangler.`,
	`%s1 dit à %s2 : « Tu vois, le monde se divise en deux catégories : ceux qui ont un pistolet chargé et ceux qui creusent. Toi, tu creuses. »`,
	`%s1 lance sa voiture sur %s2 et il n'a pas le permis en plus ! « Va sucer ton gilet jaune en enfer %s2 ! »`,
	`%s1 défie %s2 à la roulette russe :gun: et... gagne :white_check_mark:. Champion ! Il retourne ensuite son pistolet contre %s2 et lui loge une balle en pleine tête. Adieu loser !`,
	`%s1 se cache dans les buissons et prépare son attaque surprise. Dès que %s2 se rapproche du point fatidique, %s1 se jette sur lui, mais, dans son élan, ne voit pas la bouche d'égout ouverte... %s2 jette un œil mais ne voit plus qu'une masse inerte au sol !`,
	`%s2 se prends pour une 8ème Merveille. Il est le seul à ne pas savoir que c'est %s1 qui a signé le brevet juste avant. Quel imbécile ce %s2 ! :person_facepalming:`,
	`%s1 et %s2 se lance dans un concours de *lookaway* <:1_lookaway:596441749166227466>. Après plusieurs minutes à se fixer dans les yeux sans rompre, %s1 simule un vomissement sur %s2 qui détourne le regard. %s1 en profite pour lui arracher les :eyes: avec une paire de :scissors: avant de les brandir fièrement en l'air. **FATALITY!**`,
	`%s1 donne un cupcake à %s2. *C'est une spécialité maison, devine quel parfum c'est !*. %s2 le met dans sa bouche.... **C'est du C4 MODAFUCKA !!!**. %s1 n'a plus qu'à ramasser ensuite les petits morceaux collés aux murs`,
	`%s1 envoie une galette trop cuite sur %s2 qui se retrouve masqué comme la lune !`,
	`%s1 décide de sortir son biniou et les tympans de %s2 se mettent à saigner abondamment :drop_of_blood: !`
];

const reponsesAdversaireVainqueur = [
	`%s1 attaque %s2 mais celui-ci est plus rapide et parvient à esquiver. %s2 riposte et envoie une roquette sur %s1 qui se fait vaporiser :boom:.`,
	`%s1 défie %s2 à la roulette russe :gun: et... perd :x:. Quel nul !`,
	`Un duel au sabre laser s’engage. %s1, tente d'acculer son adversaire, mais %s2 semblait avoir prévu le coup, et arrive rapidement à retourner la situation à son avantage. %s1 trébuche sur un Artefact en bois qui trainait par terre, tombe, et %s2 en profite pour le découper en deux au niveau de la taille. %s1 disparait, seuls ses vêtements restent sur le sol.`,
	`%s1 dit a %s2: « Tu vois, le monde se divise en deux catégories : ceux qui ont un pistolet chargé et ceux qui creusent. Toi, tu creuses. » Mais %s2 donne un coup de pelle dans les dents de %s1, récupère son arme, et l'abat froidement. %s1 tombe dans le trou, et %s2 l'enterre proprement.`,
	`%s1 fonce sur %s2, mais celui-ci l'évite au dernier moment. %s1 se fracasse la tête contre le mur et s'évanouit.`,
	`%s1 dégoupille sa grenade, et dans l'euphorie, lance la goupille à %s2... :boom: %s1 explose !`,
	`%s1 à eu un accès de colère contre %s2 mais se dit que finalement... A quoi bon ? %s1 fait un calin à %s2 :hugging: :kiss:`,
	`%s1 se lance à la poursuite de %s2. Durant cette folle épopée sur les plaines de Gorgoroth qui dura plusieurs jours, les deux forcenés s'en donnèrent à cœur-joie, se mettant des gnons dans la tronche à qui mieux-mieux pour finalement trébucher au bord de l'Oroduin et tomber ensemble dans les flammes du volcan :fire:. Tout ça pour ça %s1 et %s2... Tout ça pour ça...`,
	`%s1 glisse un :snake: dans le lit de %s2 pendant qu'il dort mais le :snake: se rebiffe et mord %s1. Sans anti-venin sur lui, %s1 meurt stupidement et %s2 le retrouve, raide, au petit matin.`,
	`Outré que %s1 touche à son playmat tout neuf, %s2 lui saute dessus et l'envoie au tapis`,
	`Parti chercher du bois, %s1 a laissé seul %s2 qui a mangé tout le pain elfique avec du beurre de cacahuète. C'est d'un distingué...`,
	`Une légende raconte que %s1, magicien à moitié sourd (mais pas à moitié con) comprit mal ce que %s2 lui avait dit lors d'un cocktail pour seniors. En effet au lieu d'entendre "ça roule, man ?" il comprit qu'on le comparait à un espèce de vieux pervers rachitique qui reste cloîtré dans sa tour blanche à caresser des boules. Résultat: un apéro gâché, un incendie à gérer et la honte devant tout le monde pour %s1 :clap:`
];

const reponsesCollateral = [
	`%s2 a mal compris les consignes de %s1 et a finalement pris trois fois à droite au lieu d'aller tout droit puis à gauche, puis à droite, encore à gauche, en zigzag jusqu'au pont maudit et ensuite tout droit deux fois et enfin trois pas en arrière pour bifurquer vers la gauche. C'était quand même très clair, même %s3 a capté au moins la moitié ! Du coup, %s2 s'est perdu dans la grotte de glace à tout jamais.`,
	`%s1 et %s2 décident de faire une partie de Talisman (édition 1994 de Gallimard Jeunesse), pas de chance pour %s3 qui entra dans la salle de jeu à ce moment-là. Obligé de jouer, il fini par convulser au bout de trois tours de table après avoir était dans l'incapacité de sortir du coin en haut à droite du plateau... Triste fin pour %s3 !`,
	`Alors que %s1 et %s2 entament une partie de jeux de société autour d'une table, une dispute éclate autour du choix du personnage. %s2 éclate de rage et retourne violemment la table en faisant voler dans toute la salle les éléments du jeu. Pas de bol pour %s3 qui se trouvait sur la table d'en face et qui bâillait à ce moment-là, la figurine va se coincer au fond de sa gorge, il meurt sur le coup d'une asphyxie. Fin de la partie !"`
];
