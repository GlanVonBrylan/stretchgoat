"use strict";

const { EmbedBuilder } = require("discord.js");

exports.description = "Afficher plein de liens utiles";
exports.run = inter => inter.reply({embeds: [
	(inter.locale.startsWith("en") ? embed_en : embed).setThumbnail(inter.guild.iconURL())
]});

exports.localization = {
	name: "links", description: "Display a bunch of useful links",
};



const embed = new EmbedBuilder()
	.setTitle("Liens utiles sur The 7th continent")
	.setColor(bot.config.blue)
	.addField("Forum officiel", "https://the7thcontinent.seriouspoulp.com/fr/forum/")
	.addField("Page officielle KS", "https://www.kickstarter.com/projects/1926712971/the-7th-continent-explore-survive-you-are-the-hero")
	.addField("Page officielle KS2", "https://www.kickstarter.com/projects/1926712971/the-7th-continent-what-goes-up-must-come-down")
	.addField("Carte Google Maps des joueurs du 7th Continent", "https://www.google.com/maps/d/edit?mid=1helLLO2VF3C9ujn6G6coU9E2tB8&ll=47.919224928039945%2C3.596496206760321&z=5")
	.addField("Groupe Facebook Explore le 7ème Continent [7th Continent]", "https://www.facebook.com/groups/2046584498950532/");


const embed_en = new EmbedBuilder()
	.setDescription("Useful links about The 7th Continent")
	.setColor(bot.config.blue)
	.addField("Official forum", "https://the7thcontinent.seriouspoulp.com/en/forum/")
	.addField("First KS", "https://www.kickstarter.com/projects/1926712971/the-7th-continent-explore-survive-you-are-the-hero")
	.addField("Second KS", "https://www.kickstarter.com/projects/1926712971/the-7th-continent-what-goes-up-must-come-down");
