"use strict";

const tags = exports.tags = {};

Object.defineProperty(tags, "toChoices", {value: function() {
	const choices = Object.keys(tags).map(name => ({name, value: name}));
	return choices.length ? choices : [{name: "*Il n'y a aucun tag.*", value: "/"}];
}});


const saveData = require("../../data/singleFileDataSaver").getSync("tags.json", data => {
	Object.assign(tags, data.authorized ? data.tags : data);
}, 2).bind(null, tags);


exports.description = "Gérer les tags";
exports.defaultMemberPermissions = "0";
exports.options = [{
	type: SUBCOMMAND, name: "cree",
	description: "Créer un tag",
	options: [{
		type: STRING, name: "nom", required: true,
		description: "Le nom du tag",
	}, {
		type: STRING, name: "contenu", required: true,
		description: "Le texte qui sera envoyé lors de l'utilisation du tag. Remplacez les retours à la ligne par ###",
	}],
}, {
	type: SUBCOMMAND, name: "supprime",
	description: "Supprimer un tag",
	options: [{
		type: STRING, name: "nom", required: true,
		description: "Le nom du tag à supprimer",
		choices: tags.toChoices(),
	}],
}, {
	type: SUBCOMMAND, name: "liste",
	description: "Lister les tags existants",
},
{
	type: SUBCOMMAND, name: "autorisation",
	description: "Gérer les utilisateurs autorisés à utiliser les tags",
}];
exports.run = inter => {
	const {id} = inter.user;
	const cmdGroup = inter.options.getSubcommandGroup(false);
	const cmd = inter.options.getSubcommand();

	if(cmd === "autorisation")
		inter.reply({flags: "Ephemeral", content: "Vous pouvez désormais gérer ça dans les paramètres du serveur ! Allez pour cela dans Intégrations et cliquez sur moi."});
	else if(cmd === "liste")
	{
		const fields = Object.entries(tags).map(([name, tag]) => {
			const weightedLength = tag.length + 4 * (tag.match(/\n/g)?.length || 0); // line breaks count for 5
			return { name: "🏷️ "+name,
				value: `${weightedLength < 200 ? tag : `${tag.substring(0, 190)}…\n*… et encore ${tag.length - 190} caractères*`}`
			};
		});
		inter.reply({embeds: [{
			title: "Liste des tags",
			...(fields.length ? {fields} : {description: "*aucun*"})
		}]});
	}
	else
	{
		const nom = inter.options.getString("nom");
		if(cmd === "cree")
		{
			if(Object.keys(tags).length === 25)
				inter.ephReply("Il y a déjà 25 tags, ce qui est le maximum.");
			else if(nom in tags)
				inter.ephReply("Ce tag existe déjà.");
			else if(nom.length > 32)
				inter.ephReply("Le nom des tags ne peut pas dépasser 32 caractères.");
			else
			{
				tags[nom] = inter.options.getString("contenu").replaceAll("###", "\n");
				tagsChanged();
				inter.reply("Tag ajouté.");
			}
		}
		else if(cmd === "supprime")
		{
			if(nom === "/")
				inter.ephReply("Il n'y a aucun tag.");
			else if(nom in tags)
			{
				delete tags[nom];
				tagsChanged();
				inter.reply("Tag supprimé.");
			}
			else
				inter.ephReply("Ce tag n'existait pas.");
		}
		else
			console.error(`/${exports.name}: unknown subcommand received: ${cmd}`);
	}
}


function tagsChanged()
{
	const {options: [tag]} = exports.options.find(({name}) => name === "supprime");
	tag.choices = tags.toChoices();
	exports.self.edit(exports);
	require("./tag").updateTags();
	saveData();
}
