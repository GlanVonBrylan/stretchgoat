"use strict";

require("../../utils/array.js");
require("../../utils/replaceLast.js");

const EMOJI_REGEX = /^(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|\ud83c[\ude32-\ude3a]|\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])$/;

const DEFAULT_COLOR = "#e5e1e1";
const MAX_LIMIT = 1206000_000; // 14 jours, moins 1 heure pour être sûr

const {
	DEFAULT_EMOJI,
	distribute, groupPrizes,
	createGiveaway, endGiveaway,
} = require("./#functions/giveaway");


function joinNames(names)
{
	const l = names.length - 1;
	return l ? `${names.slice(0, l).join(", ")} et ${names[l]}` : names[0];
}


exports.description = "Faire un giveaway";
exports.defaultMemberPermissions = "0";
exports.options = [{
	type: SUBCOMMAND, name: "participatif",
	description: "Faire un giveway où les gens participeront en mettant une réaction.",
	options: [{
		type: STRING, name: "titre", required: true,
		description: "Titre du giveaway",
	}, {
		type: STRING, name: "duree", required: true,
		description: "Jours ou heures avant tirage au sort, par exemple 6h ou 3j. Doit être entre 1h et 14j.",
	}, {
		type: STRING, name: "prix", required: true,
		description: "Liste des prix, séparés par des virgules.",
	}, {
		type: STRING, name: "texte", required: true,
		description: "Texte du giveaway. Remplacez les retours à la ligne par ###",
	}, {
		type: STRING, name: "emoji",
		description: `L'émoji utilisée pour participer (par défaut ${DEFAULT_EMOJI})`,
	}, {
		type: STRING, name: "couleur",
		description: `Code hexadécimal de la couleur de l'embed (par défaut ${DEFAULT_COLOR})`
	}],
}, {
	type: SUBCOMMAND, name: "immediat",
	description: "Faire un giveaway à tirage immédiat",
	options: [{
		type: STRING, name: "prix", required: true,
		description: "Liste des prix, séparés par des virgules.",
	}, {
		type: STRING, name: "participants", required: true,
		description: "Liste de noms séparés par des virgules ou de mentions.",
	}, {
		type: STRING, name: "distribuer-tout",
		description: "S'il faut distribuer tous les prix même s'il y en a plus que de participants. (par défaut : non)",
		choices: [{ name: "Oui", value: "oui" }, { name: "Non", value: "non" }],
	}],
}];
exports.run = async inter => {
	const {options} = inter;
	if(options.getSubcommand() === "immediat")
	{
		const distributeAll = options.getString("distribuer-tout") === "oui";
		const prizes = options.getString("prix").split(",").map(prize => prize.trim()).filter(Boolean);
		if(!prizes.length)
			return inter.ephReply("Vous n’avez indiqué aucun prix...");

		let names = options.getString("participants").split(",").map(name => name.trim()).filter(Boolean);
		if(!names.length)
			return inter.ephReply("Vous n’avez indiqué aucun nom...");

		if(names.length === 1) // Sans doute des mentions !
			names = names[0].match(/<@!?[0-9]+>/g);

		const distribution = distribute(prizes, names.map(id => ({ id })), distributeAll);
		const embed = { color: +DEFAULT_COLOR.replace("#", "0x"), title: "Tirage terminé !" };

		if(prizes.length === 1)
			embed.description = `Et c’est ${distribution.id} qui remporte un·e magnifique **${prizes[0]}** !`;
		else
		{
			embed.description = "Résultats du giveaway";
			embed.fields = Object.entries(distribution).map(prizes.tooMany && distributeAll
				? ([name, prizes]) => ({ name: name, value: prizes.join(", "), inline: true })
				: ([name, prize]) => ({ name: prize, value: name, inline: true }));
		}

		if(prizes.tooMany && !distributeAll)
			embed.footer = {text: `Par manque de participants, les prix suivants n’ont pas été distribués : ${groupPrizes(prizes)}`};
		else if(prizes.losers)
		{
			const losers = prizes.losers.map(({id}) => id);
			for(let i = 0 ; i < losers.length ; i++)
				if(losers[i].match(/<@!?[0-9]+>/))
					losers[i] = (await inter.guild.members.fetch(losers[i].match(/[0-9]+/)[0])).displayName;
			embed.footer = { text: `${joinNames(losers)} n’${prizes.losers.length === 1 ? "a" : "ont"} rien gagné !` };
		}

		inter.reply({ embeds: [embed] });
		return;
	}

	const title = options.getString("titre");
	const emoji = options.getString("emoji") || DEFAULT_EMOJI;
	let color = options.getString("couleur") || DEFAULT_COLOR;
	let limitPrm = options.getString("duree");

	if(!/^<a?:[\wÀ-ÿ]+:[0-9]+>$/.test(emoji) && !EMOJI_REGEX.test(emoji))
		return inter.ephReply("Émoji invalide.");

	color = color.toLowerCase();
	if(color.length === 6)
		color = "#"+color;

	if(!color.match(/^#[0-9a-f]+$/))
		return inter.ephReply("Format de couleur invalide.");

	color = Number(color.replace("#", "0x"));

	let limit;
	if(/^[0-9]{1,3}[jh]$/.test(limitPrm))
	{
		if(limitPrm === "14j" || limitPrm === "336h")
			limit = MAX_LIMIT;
		else
		{
			limit = ~~(parseFloat(limitPrm) * 3600_000);
			if(limitPrm[limitPrm.length - 1] === "j")
				limit *= 24;
		}

		if(limit < 3600_000 || limit > MAX_LIMIT)
			return inter.ephReply("Le nombre de jours avant le tirage au sort doit être compris entre 1h et 14j.");
	}
	else
		return inter.ephReply("Durée invalide.");

	const prizes = options.getString("prix").split(",").map(prize => prize.trim()).filter(Boolean);
	const nPrizes = prizes.length;
	if(!nPrizes)
		return inter.ephReply("Vous devez indiquer au moins un prix. Le nom des prix doivent être séparés par des virgules, et ne peuvent donc pas en contenir.");
	if(nPrizes > 25)
		return inter.ephReply("Vous ne pouvez pas offrir plus de 25 prix à la fois.");

	limitPrm = [parseInt(limitPrm), limitPrm[limitPrm.length - 1].replace("j", "jour").replace("h", "heure")];
	if(limitPrm[0] !== 1)
		limitPrm[1] += "s";


	const msg = await inter.reply({
		content: "@everyone",
		embeds: [{
			color, title: `${emoji} ${title}`,
			description: `${options.getString("texte").replaceAll("###", "\n")}\n
Prix à gagner : ${groupPrizes(prizes)}
Réagissez avec ${emoji} pour participer. ${nPrizes === 1 ? "Le/la gagnant·e sera tiré·e" : "Les gagnant·e·s seront tiré·e·s"} dans ${limitPrm[0]} ${limitPrm[1]} !`,
			footer: { icon_url: inter.member.displayAvatarURL(), text: `Lancé par ${inter.member.displayName} •• Se finit` },
			timestamp: new Date(Date.now() + limit).toISOString(),
		}],
		allowedMentions: {parse: ["users", "roles", "everyone"]},
	})
		.then(reply => reply.fetch())
		.catch(console.error);

	try {
		await msg.react(emoji).catch(console.error);
		createGiveaway(msg, Date.now() + limit, prizes, emoji, color);
	} catch(err) {
		if(err.message === "Unknown Emoji")
			inter.channel.send("Je ne connais malheureusement pas cette émoji...");
		else
		{
			msg?.delete();
			inter.channel.send(`${inter.user} J'ai pas réussi à ajouter la réaction ;-;`);
			console.error(err);
		}
	};
}
