"use strict";

const { ManageMessages: MANAGE_MESSAGES } = require("discord.js").PermissionFlagsBits;

exports.description = "Supprimer plusieurs messages d'un coup";
exports.defaultMemberPermissions = MANAGE_MESSAGES;
exports.options = [{
	type: INTEGER, name: "nombre", required: true,
	description: "Le nombre de messages à supprimer.",
	minValue: 1, maxValue: 100,
}];
exports.run = async inter => {
	if (!inter.member.permissions.has(MANAGE_MESSAGES))
		return inter.ephReply("Vous n'avez pas la permission de gérer les messages.");
	if (!inter.guild.members.me.permissions.has(MANAGE_MESSAGES))
		return inter.ephReply("Je n'ai pas la permission de gérer les messages.");

	const n = inter.options.getInteger("nombre");

	const [, {status, reason}] = await Promise.allSettled([inter.deferReply({flags: "Ephemeral"}), inter.channel.bulkDelete(n)]);
	inter.editReply(status === "fulfilled"
		? `${n} messages supprimés`
		: `Échec de la suppression de messages : ${reason}`
	);
}
