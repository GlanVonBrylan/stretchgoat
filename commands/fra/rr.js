"use strict";

const {writeFile} = require("fs/promises");
const { ChannelType: {GuildText: GUILD_TEXT, GuildNews: GUILD_NEWS} } = require("discord.js");

const {roles} = bot;
function saveData() { return writeFile("./data/roles.json", JSON.stringify(roles, null, 4)); }


function dspEmoji(emoji) {
	return /[0-9]/.test(emoji) ? `<:${emoji}>` : emoji;
}

exports.description = "Gérer les réactions à rôles";
exports.defaultMemberPermissions = "0";
exports.options = [{
	type: SUBCOMMAND, name: "ajoute",
	description: "Ajouter une réaction à rôle",
	options: [{
		type: CHANNEL, name: "salon", required: true,
		channelTypes: [GUILD_TEXT, GUILD_NEWS],
		description: "Le salon où se trouve le message où sera la réaction",
	}, {
		type: STRING, name: "message", required: true,
		description: "L'id du message où mettre la réaction",
	}, {
		type: STRING, name: "emoji", required: true,
		description: "L'émoji",
	}, {
		type: ROLE, name: "role", required: true,
		description: "Le rôle qui sera lié à la réaction",
	}],
}, {
	type: SUBCOMMAND, name: "supprime",
	description: "Supprimer une réaction à rôle",
	options: [{
		type: CHANNEL, name: "salon", required: true,
		channelTypes: [GUILD_TEXT, GUILD_NEWS],
		description: "Le salon où se trouve le message où est la réaction",
	}, {
		type: STRING, name: "message", required: true,
		description: "L'id du message où est la réaction",
	}, {
		type: STRING, name: "emoji",
		description: "L'émoji ; laissez vide pour supprimer toutes les réactions du message",
	}],
}, {
	type: SUBCOMMAND, name: "liste",
	description: "Lister les réactions à rôles",
}];
exports.run = async inter => {
	const {options} = inter;

	const cmd = options.getSubcommand();
	if(cmd === "liste") {
		const baseURL = `https://discord.com/channels/${inter.guild.id}/`;
		const embeds = Object.entries(roles).map(([msgId, {channel, reactions}]) => ({
			description: `<#${channel}> / [${msgId}](${baseURL}${channel}/${msgId})`,
			fields: reactions.map(({emoji, role}) => ({name: dspEmoji(emoji), value: `<@&${role}>`, inline: true})),
		}));

		if(!embeds.length)
			inter.reply("Il n'y a aucune réaction à rôle.");
		else if(embeds.length < 11)
			inter.reply({embeds});
		else
		{
			await inter.reply({embeds: embeds.slice(0, 10)}).catch(console.error);
			for(let start = 10 ; start < embeds.length ; start += 10)
				inter.channel.send({embeds: [embeds.slice(start, start + 10)]});
		}

		return;
	}

	const channel = options.getChannel("salon");
	const msgId = options.getString("message");

	const msg = await channel.messages.fetch(msgId).catch(Function());
	if(!msg) return inter.reply("L'id du message n'est pas valide.");

	const emoji = options.getString("emoji");
	const role = options.getRole("role");

	if(cmd === "ajoute")
		addReaction();
	if(cmd === "supprime")
		deleteReaction();


	function addReaction() {
		let cancel;
		if(msgId in roles) {
			if(emojiExists(msgId, emoji))
				return inter.reply("Cet emoji existe déjà pour ce message.");

			roles[msgId].reactions.push({ emoji, role: role.id });
			cancel = () => roles[msgId].reactions.pop();
		} else {
			roles[msgId] = { channel: channel.id, reactions: [ { emoji, role: role.id } ] };
			cancel = () => delete roles[msgId];
		}

		doReactions().then(() => {
			saveData();
			inter.reply("Le rôle a bien été ajouté à ce message.");
		}, err => {
			cancel();
			console.error(err);
			return inter.reply("Une erreur est survenue.");
		});
	}

	function doReactions() {
		return Promise.all(roles[msgId].reactions.map(({emoji}) => {
			emoji = bot.cleanEmojiDiscriminator(emoji);
			const messageReaction = msg.reactions.cache.get(emoji);
			if(!messageReaction || !messageReaction.users.cache.has(bot.user.id))
				return msg.react(emoji);
		}));
	}


	function deleteReaction() {
		if(!(msgId in roles))
			return inter.reply("Le message spécifié est introuvable dans le fichier. Il a sans doute déjà été supprimé.");

		if(!emoji) {
			for(const {emoji} of roles[msgId].reactions) {
				const messageReaction = msg.reactions.cache.get(emoji);
				if(messageReaction)
					messageReaction.users.remove().catch(Function());
			}
			delete roles[msgId];
			saveData();
			inter.reply("Tous les couples emoji/rôle ont été supprimés de ce message.");
		} else if(emojiExists(msgId, emoji)) {
			const messageReaction = msg.reactions.cache.get(emoji);
			if(messageReaction)
				messageReaction.users.remove().catch(Function());

			let reply;
			if(roles[msgId].reactions.length > 1) {
				const index = roles[msgId].reactions.findIndex(({emoji: e}) => e === emoji);
				roles[msgId].reactions.splice(index, 1);
			} else {
				delete roles[msgId];
			}

			saveData().then(
				() => inter.reply("Le couple emoji/rôle a bien été supprimé de ce message."),
				err => {
					console.error(err);
					inter.reply("Une erreur est survenue.");
			});
		}
	}
}


function emojiExists(msgId, _emoji) {
	_emoji = bot.cleanEmojiDiscriminator(_emoji); // This returns emoji without starting and ending :
	for(const {emoji} of roles[msgId].reactions)
		if(bot.cleanEmojiDiscriminator(emoji) === _emoji)
			return true;
	return false;
}
