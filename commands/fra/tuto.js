"use strict";

exports.description = "Voir un tuto pour découvrir Discord";
exports.run = inter => {
	inter.reply({embeds: [{
		title: "Tutoriel vidéo de Discord",
		thumbnail: { url: inter.guild.iconURL() },
		color: bot.config.blue,
		fields: [{ name: "Adresse :", value: "https://www.youtube.com/watch?v=Wfk-hYS0aZM" }],
	}]});
}
