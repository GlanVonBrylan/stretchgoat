"use strict";

exports.description = "Lance un dé !";
exports.options = [{
	type: INTEGER, name: "faces",
	description: "Nombre de face du dé (par défaut : 6)",
	minValue: 2, maxValue: 1000,
}, {
	type: INTEGER, name: "nombre",
	description: "Le nombre de dés à lancer (par défaut : 1)",
	minValue: 1, maxValue: 1000,
}];
exports.run = inter => {
	const faces = inter.options.getInteger("faces") || 6;
	const n = inter.options.getInteger("nombre") || 1;
	const jets = [];
	for(let i = 0 ; i < n ; i++)
		jets.push(~~(Math.random() * faces) + 1);

	const score = jets.reduce((acc, val) => acc + val, 0);
	inter.reply(inter.locale.startsWith("en")
		? `You rolled ${n === 1 ? "a " : n}🎲${faces} and got a score of 🎲**${score}**${n === 1 ? "" : `_(${jets.join("+")})_`}`
		: `Tu as lancé ${n === 1 ? "un " : n}🎲${faces} et obtenu un score de 🎲**${score}**${n === 1 ? "" : `_(${jets.join("+")})_`}`
	);
}


exports.localization = {
	name: "dice", description: "Throw some dice!",
	options: {
		faces: { name: "sides", description: "The dices' number of sides (by default 6)" },
		nombre: { name: "number", description: "The number of dice to throw (by default 1)"},
	},
};
