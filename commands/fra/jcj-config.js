"use strict";

const fs = require("fs");

if(!fs.existsSync("data/jcj/settings.json"))
	fs.writeFileSync(`data/jcj/settings.json`, "{\"limits\": {}}");


const settings = JSON.parse(fs.readFileSync("data/jcj/settings.json"));
var sortedLimits;
updatesortedLimits();

function updatesortedLimits() {
	sortedLimits = Object.entries(settings.limits).sort(([, a], [, b]) =>
		a === "infini" ? -Infinity
		: b === "infini" ? Infinity
		: b - a
	);
}


exports.getMaxJCJ = ({roles: {cache: roles}}) => {
	for(const [roleId, max] of sortedLimits)
		if(roles.has(roleId))
			return max;
	return 1;
}


exports.defaultMemberPermissions = "0";
exports.description = "Changer la limite de JcJ par jour des rôles et membres.";
exports.options = [{
	type: SUBCOMMAND, name: "voir",
	description: "Voir la configuration actuelle",
}, {
	type: SUBCOMMAND, name: "changer",
	description: "Changer la configuration",
	options: [{
		type: INTEGER, name: "quota", required: true,
		description: "Le nombre de JcJ permis par jour. De base, on peut en faire 1. Vous pouvez mettre \"infini\".",
	}, {
		type: ROLE, name: "role", required: true,
		description: "Le rôle dont il faut changer le quota",
	}],
}];
exports.run = inter => {
	if(inter.options.getSubcommand() === "voir")
	{
		let lastMax;
		const fields = [];
		for(const [roleId, max] of sortedLimits)
		{
			if(max !== lastMax)
			{
				lastMax = max;
				fields.push({name: max === "infini" ? "Infini ∞" : max === 0 ? "Interdit" : `${max} par jour`, value: `<@&${roleId}>`});
			}
			else
				fields[fields.length-1].value += ` <@&${roleId}>`;
		}

		inter.reply({embeds: [{
			color: 0xff9900,
			title: "Limites pour le JCJ",
			description: "Par défaut, les utilisateurs sont limités à 1 jcj par jour.\nSi un joueur a plusieurs rôles avec une limite custom, la limite la plus élevée est prise en compte.",
			fields
		}]});
	}
	else
	{
		let maxDaily = inter.options.getString("quota");
		const role = inter.options.getRole("role");
		if(maxDaily === "\"infini\"")
			maxDaily = "infini";
		else if(maxDaily !== "infini")
		{
			maxDaily = +maxDaily;
			if(!Number.isInteger(maxDaily) || maxDaily < 0)
				return inter.reply("La limite doit être \"infini\", 0 ou un entier positif.");
		}

		const {limits} = settings;
		if(maxDaily === 1)
			delete limits[role.id];
		else
			limits[role.id] = maxDaily;

		fs.promises.writeFile("data/jcj/settings.json", JSON.stringify(settings))
		.then(() => inter.reply("Paramètres mis à jour."))
		.catch(err => {
			console.error(err);
			inter.reply("Une erreur est survenue.");
		});

		updatesortedLimits();
	}

}
