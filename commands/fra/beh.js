"use strict";

exports.defaultMemberPermissions = "0";
exports.description = "Me faire dire quelque chose";
exports.options = [{
	type: STRING, name: "texte", required: true,
	description: "Ce que je dois dire",
}]
exports.run = inter => {
	inter.ephReply("bâââââh"); // personne ne verra ça à part la personne ayant fait la commande
	inter.channel.send(inter.options.getString("texte"));
}
