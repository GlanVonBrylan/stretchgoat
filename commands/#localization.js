"use strict";

module.exports = exports = function localizeCommand(_, command)
{
	if(command.localization)
	{
		const { localization: { name, description, options } } = command;
		command.nameLocalizations = locObj(name);
		command.descriptionLocalizations = locObj(description);

		if(options) for(const option of command.options)
		{
			const { name, description } = options[option.name];
			option.nameLocalizations = locObj(name);
			option.descriptionLocalizations = locObj(description);
		}
	}
	
	return command;
};

function locObj(string)
{
	return { "en-US": string, "en-GB": string };
}