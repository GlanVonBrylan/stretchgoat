"use strict";

const { restart } = require("./#functions");

exports.description = "Redémarre le bot"
exports.run = inter => {
	const reply = inter.reply("Redémarrage...").catch(console.error);
	restart().catch(async err => {
		console.error(err);
		await reply;
		inter.editReply(`Erreur :\n${stderr}`);
	});
}
