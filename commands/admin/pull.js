"use strict";

const { pull } = require("./#functions");

exports.description = "Tirer les changements depuis le dépôt distant.";
exports.options = [{
	type: BOOLEAN, name: "force",
	description: "force pull, par exemple pour un rollback",
}];
exports.run = async inter => {
	const [res] = await Promise.allSettled([pull(inter.options.getBoolean("force")), inter.deferReply()]);
	inter.editReply(res.status === "fulfilled" ? res.value : res.reason.stderr);
};
