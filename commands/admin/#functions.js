"use strict";

const exec = require("util").promisify(require("child_process").exec);
const { main, repository: {url} } = require("../../package.json");
const branch = url.includes("/tree/") ? url.substring(url.lastIndexOf("/tree/") + 6) : "master";


exports.install = async (ci) => (await exec(ci ? "npm ci" : "npm i")).stdout;


exports.pull = async (force = false) => "Pull réussi :\n" + (await exec(`${force
	? `git checkout ${branch} && git fetch --all -f && git reset --hard origin/${branch}`
	: "git pull"}`)).stdout;


exports.restart = () => exec("forever restart "+main);
