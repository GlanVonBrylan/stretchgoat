"use strict";

const { pull, install, restart } = require("./#functions");

exports.description = "Met à jour le bot puis le redémarre";
exports.options = [{
	type: STRING, name: "arguments",
	description: "Action spéciales",
	choices: [
		{ name: "Intégration continue (npm ci)", value: "-ci" },
		{ name: "Mise à jour forcée", value: "-f" },
		{ name: "Les deux", value: "-f -ci" },
	],
}];
exports.run = async inter => {
	const args = inter.options.getString("arguments") || "";
	const defer = inter.deferReply().catch(console.error);
	try {
		let reply = await pull(args.includes("-f"));
		await defer;
		inter.editReply(reply);
		inter.channel.send(await install(args.includes("-ci")));
		inter.channel.send("Redémarrage...");
		await restart();
	} catch(err) {
		console.error(err);
		await defer;
		inter.editReply(`Erreur :\n${err.stderr}`);
	}
}
