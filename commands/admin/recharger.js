"use strict";

const { existsSync } = require("fs");
const { commands, reload, reloadOwner } = require("@brylan/djs-commands");

exports.description = "Recharger une commande";
exports.options = [{
	type: STRING, name: "commande", required: true,
	description: "Le nom de la commande",
}];
exports.run = inter => {
	const cmd = inter.options.getString("commande");
	if(cmd !== "admin" && !existsSync(`${__dirname}/../fra/${cmd}.js`))
		return inter.reply("Aucun fichier de commande à ce nom trouvé.");

	const promise = cmd === "admin" ? reloadOwner() : reload(cmd, "fra");

	Promise.allSettled([promise, inter.deferReply()]).then(([{status, value: apiCmd}]) => {
		if(apiCmd)
		{
			const command = commands[cmd];
			command.self = apiCmd;
			command.onReady?.();
			apiCmd.edit(command);
		}

		inter.editReply(status === "fulfilled"
			? `Commande **${cmd}** rechargée.`
			: `La commande **${cmd}** n'a pas pu être rechargée.`
		);
	});
}
