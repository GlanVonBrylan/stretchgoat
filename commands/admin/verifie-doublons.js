"use strict";

const { pull, ci, restart } = require("./#functions");

exports.description = "Fait envoyer un message pour vérifier s'il y a plusieurs instances";
exports.run = async inter => {
	inter.ephReply("Vérification...");
	inter.channel.send("Ce message vérifie la présence d'instances en double. Il sera supprimé automatiquement dans 30 secondes.")
		.then(msg => setTimeout(() => msg.delete(msg).catch(Function()), 30_000));
}
