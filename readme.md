# StretchGoat

Un bot pour le serveur discord *The Seventh Dis-continent*.

Vous devez créer à la racine un dossier `data` contenant un fichier `settings.json` avec le contenu suivant :

```JSON
{
	"bot_token": "token de connexion du bot",
	"invite_url": "https://discord.gg/33Xkd9K",

	"admin_role": "id du rôle administateur",
	"role_warn": {},
	"role_emojis": {
		"get": "<:thumb:1079130576591790191>",
		"lose": "<:curse:1079130321448087592>",
		"warn": "<:hand_green:1079130386300416082>"
	},
	"log_channel": "id du salon où log l'activité des rôles-réactions",

	"red": "#990000",
	"blue": "#0099ff",
	"green": "#00ff00",
	"orange": "#ff9900",
	"purple": "#9900ff"
}
```
`role_warn` peut contenir des paires rôle:salon. Si quelqu'un reçoit le rôle correspondant grâce aux rôles-réactions, un message sera envoyé dans le salon correspondant.

## Crédits

Bot créé initialement par **Khyinn**, puis repris par **Morgân von Brylân**.

Khyinn a fait tout ce qui précède le dépôt git hormis la configuration des rôles pour le JCJ.
