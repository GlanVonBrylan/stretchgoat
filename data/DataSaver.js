const {
	existsSync,
	mkdirSync, readdirSync,
	readFileSync,
	promises: {writeFile, unlink},
} = require("fs");

const clone = require("../utils/clone");

const dataSavers = {};

module.exports = exports = function getSaver(name, defaultValue) {
	const dataSaver = dataSavers[name];
	if(dataSaver)
	{
		if(defaultValue)
		{
			if(dataSaver.defaultValue)
				console.warn(`La valeur par défaut du DataSaver "${name}" a été écrasée.`);
			dataSaver.defaultValue = defaultValue;
		}
		return dataSaver;
	}
	else
		return dataSavers[name] = new DataSaver(name, defaultValue);
};


class DataSaver extends Map {
	constructor(name, defaultValue) {
		super();

		this.name = name;
		const folder = this.folder = `${__dirname}/${name}`;
		this.defaultValue = defaultValue;
		this.writing = new Set();

		if(!existsSync(folder))
			mkdirSync(folder);

		const oldData = folder + ".json";

		if(existsSync(oldData)) {
			for(const [id, data] of Object.entries(JSON.parse(readFileSync(oldData))))
				writeFile(`${folder}/${id}.json`, JSON.stringify(data));

			unlink(oldData);
		}
	}

	reset(id) {
		return this.set(id, clone(this.defaultValue));
	}

	get({id}) {
		if(!id && id !== 0)
			throw new TypeError(`The argument must have a non-empty id; ${id === "" ? "empty string" : id} received.`);
		if(this.has(id))
			return super.get(id);

		const file = `${this.folder}/${id}.json`;
		let infos;

		if(existsSync(file)) {
			try {
				const data = readFileSync(file, "utf-8");
				if(data) infos = JSON.parse(data);
				else {
					console.warn(`Found empty file while loading ${this.name} data for user ${id}.`);
					infos = clone(this.defaultValue);
				}
			} catch(e) {
				console.error(e);
				throw new Error(`Could not load ${this.name} data for user ${id}.`);
			}
		} else {
			infos = clone(this.defaultValue);
			writeFile(file, JSON.stringify(infos));
		}

		this.set({id}, infos);
		return infos;
	}

	set({id}, data) {
		super.set(id, data);
		this.save({id});
		return this;
	}

	delete({id}) {
		if(super.delete(id))
		{
			unlink(`${this.folder}/${id}.json`, err => { if(err) console.error(err); });
			return true;
		}
		return false;
	}

	loadAll() {
		if(!this.allLoaded)
		{
			const folder = this.folder + "/";
			this.allLoaded = true;

			for(const file of readdirSync(this.folder))
			{
				if(!file.endsWith(".json") || !parseInt(file))
					continue;

				const id = file.substring(0, file.length - 5);

				if(!super.has(id))
					super.set(id, JSON.parse(readFileSync(folder + file)));
			}
		}

		return this;
	}

	save({id}) {
		if(this.writing.has(id)) {
			if(!this.writing.has(id + "timeoutScheduled")) {
				setTimeout(() => this.save({id}), 500);
				this.writing.add(id + "timeoutScheduled");
			}
		} else {
			this.writing.add(id);

			writeFile(`${this.folder}/${id}.json`, JSON.stringify(this.get({id})))
			.finally(() => {
				this.writing.delete(id);
				this.writing.delete(id + "timeoutScheduled");
			});
		}
	}
}

exports.baseDir = DataSaver.baseDir = __dirname;
