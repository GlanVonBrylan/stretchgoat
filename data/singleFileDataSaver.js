"use strict";

const { existsSync, readFileSync, promises: {readFile, writeFile} } = require("fs");


/**
 * Loads data and returns a dataSaver
 * @see loadData
 * @see getDataSaver
 */
exports.get = (fileName, recipient, spaces) => {
	exports.loadData(fileName, recipient);
	return exports.getDataSaver(fileName, spaces);
}

/**
 * get, but synchronous
 * @see get
 */
exports.getSync = (fileName, recipient, spaces) => {
	exports.loadDataSync(fileName, recipient);
	return exports.getDataSaver(fileName, spaces);
}


/**
 * Loads data from the given file and assigns it to the recipient.
 * @param {string} fileName The file name
 * @param {object|function} recipient The object to which the data will be assigned.
 	If it's a function, it will be called with the data as its argument instead.
 * @returns {Promise<object>} A Promise resolving to the recipient
 */
exports.loadData = async (fileName, recipient) => {
	if(existsSync(`${__dirname}/${fileName}`))
	{
		const data = JSON.parse(await readFile(`${__dirname}/${fileName}`, "utf-8"));
		if(typeof recipient === "function") recipient(data);
		else Object.assign(recipient, data);
	}
	return recipient;
}

/**
 * loadData, but synchronous
 * @see loadData
 */
exports.loadDataSync = (fileName, recipient) =>{
	if(existsSync(`${__dirname}/${fileName}`))
	{
		const data = JSON.parse(readFileSync(`${__dirname}/${fileName}`, "utf-8"));
		if(typeof recipient === "function") recipient(data);
		else Oject.assign(recipient, data);
	}
	return recipient;
}


/**
 * Returns a function that will allow saving data in the given file, inside the data folder.
 * @param {string} fileName The file name
 * @param {number|string} spaces What to give as JSON.stringify's 3rd argument
 * @returns {function} saveData(data)
 */
exports.getDataSaver = (fileName, spaces = 0) => {
	fileName = `${__dirname}/${fileName}`;
	let saving = false;
	return function saveData(data) {
		if(saving) {
			if(saving === true)
				saving = setTimeout(saveData, 500);
		} else {
			saving = true;
			writeFile(fileName, JSON.stringify(data, null, spaces))
				.finally(() => saving = false);
		}
	}
}
