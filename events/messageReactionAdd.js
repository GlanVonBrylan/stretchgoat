"use strict";

const {
	role_warn = {}, log_channel,
	role_emojis: { get: e_get = "", lose: e_lose = "", warn: e_warn = "" },
} = bot.config;

const notif = {
	"default": {
		given: "tu as maintenant le rang",
		removed: "je t'ai retiré le rang",
	},
	"en": {
		given: "you now have the rank",
		removed: "I took away the rank",
	}
};

module.exports = async (messageReaction, user) => {
	if(user.bot) return;

	const member = await messageReaction.message.guild.members.fetch(user.id);
	const emojiDiscriminator = bot.getEmojiDiscriminator(messageReaction.emoji);
	const channelId = messageReaction.message.channel.id;

	for (const [messageId, {channel, reactions}] of Object.entries(bot.roles)) {
		if (channel !== channelId) continue;

		for (let {emoji, role} of Object.values(reactions)) {
			emoji = bot.cleanEmojiDiscriminator(emoji);

			if (emojiDiscriminator !== emoji)
			 	continue;

			messageReaction.users.remove(user);
			const {roles} = member;

			let log;

			const msgChannel = await bot.channels.fetch(channel).catch(console.error);
			if(!msgChannel) return;
			const msg = await msgChannel.messages.fetch(messageId).catch(console.error);
			if(!msg) return;
			const roleName = (await msg.guild.roles.fetch(role))?.name;
			const tr = roleName?.includes("🇬🇧") ? notif.en : notif.default;

			if(!roleName) {
				log = `Rôle pour l'émoji ${messageReaction.emoji} non trouvé.`;
			} else if (roles.cache.has(role)) {
				await roles.remove(role).catch(console.error);
				log = `${e_lose} ${member} vient de perdre le rang **${roleName}**`;
				member.send(`${e_lose} ${tr.removed} **${roleName}**`);
			} else {
				await roles.add(role).catch(console.error);
				log = `${e_get} ${member} vient de recevoir le rang **${roleName}**`;
				if (role in role_warn) {
					const chan = await bot.channels.fetch(role_warn[role]);
					chan?.send(`${member} vient de rejoindre le clan des __${roleName}s__ ! Beeehvenue parmi nous ${e_warn}`);
				}
				else
					member.send(`${e_get} ${tr.given} **${roleName}**`);
			}

			if(log_channel)
			{
				const chanLog = await bot.channels.fetch(log_channel).catch(console.error);
				if (chanLog && roleName) chanLog.send(log);
			}

			return;
		}
	}
}
