"use strict";

const { links, unlinks } = require("../commands/fra/lr");
const { uTipeur, fChat } = require("../data/settings.json");
const messages = [ // {tipeur·se} sera automatiquement remplacé par une mention de la personne concernée
	"Bip, merci {tipeur·se}, bienvenue à toi et merci pour ton grand soutien !",
	"Toute l'équipe de Myludo te remercie {tipeur·se} ! Bienvenue parmi nous.",
];

module.exports = async ({roles: {cache: oldRoles}}, member) => {
	const { roles, roles: { cache } } = member;

	for(const newRole of [...cache.keys()].filter(id => !oldRoles.has(id)))
	{
		if(newRole === uTipeur)
			bot.channels.fetch(fChat)
			.then(fChat => fChat.send(messages.random().replace("{tipeur·se}", member)));

		let { [newRole]: linkedRoles } = links;
		if(!linkedRoles) continue;

		linkedRoles = linkedRoles.filter(id => !cache.has(id));
		if(linkedRoles.length)
			roles.add(linkedRoles, "Rôles liés");
	}

	for(const oldRole of [...oldRoles.keys()].filter(id => !cache.has(id)))
	{
		let { [oldRole]: linkedRoles } = unlinks;
		if(!linkedRoles) continue;
		
		linkedRoles = linkedRoles.filter(cache.has.bind(cache));
		if(linkedRoles.length)
			roles.remove(linkedRoles, "Rôles liés");
	}
}
