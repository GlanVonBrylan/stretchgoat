"use strict";

require("../utils/array.js");

var activityInterval;

module.exports = async () => {
	console.log(`Connecté en tant que ${bot.user.tag}`);

	if(activityInterval)
		bot.clearInterval(activityInterval);

	activityInterval = setInterval(() => {
		bot.user.setActivity(bot.activities.random());
	}, 86400_000); // 24h
	bot.user.setActivity(bot.activities.random());


	const guild = bot.guilds.cache.first();
	if(!guild)
		throw new Error("Could not fetch the guild");

	guild.members.fetch(); // Otherwise guildMemberUpdate does not fire properly

	const commandLoader = require("@brylan/djs-commands");
	commandLoader(bot, {
		singleServer: guild,
		ownerCommand: { name: "admin", description: "Commandes qui contrôlent le bot" },
		autoSubCommands: false,
		middleware: require("../commands/#localization"),
	}).then(apiCommands => {
		const { commands } = commandLoader;
		for(const apiCmd of apiCommands.values())
		{
			const cmd = commands[apiCmd.name];
			cmd.self = apiCmd;
			cmd.onReady?.();
		}
	});


	// Init reaction roles
	for (let [message, {channel, reactions}] of Object.entries(bot.roles))
	{
		channel = await bot.channels.fetch(channel).catch(console.error);
		if(!channel) continue;
		const msg = await channel.messages.fetch(message).catch(console.error);
		if (!msg) continue;

		for (let {emoji} of Object.values(reactions)) {
			emoji = bot.cleanEmojiDiscriminator(emoji);
			const messageReaction = msg.reactions.cache.get(emoji);
			if (messageReaction && messageReaction.count > 1) {
				await messageReaction.remove().catch(console.error);
				msg.react(emoji);
			} else if (!messageReaction || !messageReaction.me) {
				msg.react(emoji);
			}
		}
	}
};
